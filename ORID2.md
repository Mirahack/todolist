O:Today, I mainly learned about Redux and used it as a tool for unified state management to replace the states in various components from yesterday. In addition to Redux, today I gained a rough understanding of the methods of front-end unit testing.

R:I feel hot.

I:I am not very familiar with the Redux related content I learned today, and I need to invest a lot of energy to solve problems and complete requirements, trying my best to keep up with the teacher's pace, so my brain needs to run at full speed. In addition, the teacher's air conditioning seems to be broken, which makes my day very hot.

D:Although Redux, which I learned today, is not very difficult to use, I understand that it is not a simple and easy-to-use tool, and there are many functions that I have not yet explored. I hope to continue learning this tool in depth in the future.