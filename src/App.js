import { Outlet } from 'react-router-dom';

import './App.css';
import Navigater from './components/Navigater';

function App() {
  return (
    <div className="App">
      <Navigater></Navigater>
      <Outlet></Outlet>
    </div>
  );
}

export default App;
