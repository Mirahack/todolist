import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import useTodo from '../../hooks/useTodo'

const DetailPage = () => {

    const { id } = useParams()

    const [item, setItem] = useState({ id: 1, text: "" })

    const { getTodoItem } = useTodo()

    const fetch = async () => {
        const data = await getTodoItem(id)
        setItem(data)
    }

    useEffect(() => {
        fetch()
    }, )

    return (
        <div>
            <span>{item.id}</span><br></br>
            <span>{item.text}</span>
        </div>
    )

}

export default DetailPage