import React from 'react'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'

const DonePage = () => {

    const doneList = useSelector((state) => state.todo.itemList.filter(item => item.done))

    const nagivate = useNavigate()

    const handleClickDone = (id) => {
        nagivate(`../detail/${id}`)
    }

    return (
        <div>
            {doneList.map(item => 
                <div key={item.id}>
                    <input value={item.text} onClick={() => handleClickDone(item.id)} readOnly></input>
                </div>
            )}
        </div>
    )
}

export default DonePage