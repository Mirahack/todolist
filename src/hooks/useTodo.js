import { useDispatch } from "react-redux"
import { deleteTodo, getAll, getTodo, insertTodo, updateTodoStatus, updateTodoText } from "../apis/todoApi"
import { addItem, updateAllItems } from "../redux/todoSlice"

const useTodo = () => {

    const dispatch = useDispatch()

    const getAllTodoItem = async () => {
        const { data } = await getAll()
        dispatch(updateAllItems(data))
    }

    const createTodoItem = async (text) => {
        const { data } = await insertTodo(text)
        dispatch(addItem(data))
    }

    const updateTodoItemStatus = async (id, done) => {
        await updateTodoStatus(id, done)
        getAllTodoItem()
    }

    const deleteTodoItem = async (id) => {
        await deleteTodo(id)
        getAllTodoItem()
    }

    const updateTodoItemText = async (id, text) => {
        await updateTodoText(id, text)
        getAllTodoItem()
    }

    const getTodoItem = async (id) => {
        const {data} = await getTodo(id)
        return data
    }

    return {
        getAllTodoItem,
        createTodoItem,
        updateTodoItemStatus,
        deleteTodoItem,
        updateTodoItemText,
        getTodoItem
    }

}

export default useTodo