import { createSlice } from '@reduxjs/toolkit'

export const todoSlice = createSlice({
  name: 'todo',
  initialState: {
    itemList: [],
    open: false,
    item: {}
  },
  reducers: {
    addItem: (state, action) => {
      state.itemList = [...state.itemList, action.payload]
    },
    updateStatus: (state, action) => {
      state.itemList.map(item => {
        if (item.id === action.payload) {
          item.done = !item.done
        }
      })
    },
    deleteItem: (state, action) => {
      state.itemList = state.itemList.filter(item => item.id != action.payload)
    },
    updateAllItems: (state, action) => {
      state.itemList = action.payload
    },
    toggleUpdateModal: (state) => {
      state.open = !state.open
    },
    setItem: (state, action) => {
      state.item = action.payload
    },
    updateText: (state, action) => {
      state.itemList.map(item => {
        if (item.id === action.payload.id) {
          item.text = action.payload.text
        }
      })
    },
  },
})

export const { addItem, updateStatus, deleteItem, updateAllItems, toggleUpdateModal, setItem, updateText } = todoSlice.actions

export default todoSlice.reducer