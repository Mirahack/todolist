import axios from "axios"

const instance = axios.create({
    baseURL: 'http://localhost:8080'
})

export const getAll = () => instance.get('/todos')

export const insertTodo = (text) => {
    return instance.post('/todos', {
        text: text,
        done: false
    })
}

export const updateTodoStatus = (id, done) => {
    return instance.put(`/todos/${id}`, {
        done: done
    })
}

export const deleteTodo = (id) => {
    return instance.delete(`/todos/${id}`)
}

export const updateTodoText = (id, text) => {
    return instance.put(`/todos/${id}`, {
        text: text
    })
}

export const getTodo = (id) => instance.get(`/todos/${id}`)
