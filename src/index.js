import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import store from './redux/store';
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import HelpPage from './pages/helpPage/HelpPage';
import ErrorPage from './pages/errorPage/ErrorPage';
import DonePage from './pages/donePage/DonePage';
import DetailPage from './pages/detailPage/DetailPage'
import TodoList from './components/TodoList';

const root = ReactDOM.createRoot(document.getElementById('root'));
const router = createBrowserRouter([
  {
    path: "/",
    element: <App></App>,
    children: [
      {
        index: "/",
        element: <TodoList></TodoList>
      },
      {
        path: "/help",
        element: <HelpPage></HelpPage>
      },
      {
        path: "/done",
        element: <DonePage></DonePage>
      },
      {
        path: "/detail/:id",
        element: <DetailPage></DetailPage>
      },
      {
        path: "/*",
        element: <ErrorPage></ErrorPage>
      },
    ]
  },
])
root.render(
  <Provider store={store}>
    <RouterProvider router={router} />
  </Provider>
);

reportWebVitals();
