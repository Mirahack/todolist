import TodoGroup from '../TodoGroup'
import TodoGenerator from '../TodoGenerator'
import './index.css'
import { useEffect } from 'react'
import useTodo from '../../hooks/useTodo'
import TodoUpdater from '../TodoUpdater/TodoUpdater'

const TodoList = () => {

  const { getAllTodoItem } = useTodo()

  const fetch = async () => {
    getAllTodoItem()
  }

  useEffect(() => {
    fetch()
  }, [])

  return (
    <div>
        <TodoGroup></TodoGroup>
        <TodoGenerator></TodoGenerator>
        <TodoUpdater></TodoUpdater>
    </div>
  )
}

export default TodoList