import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import './index.css';
import useTodo from '../../hooks/useTodo';
import { Button, Typography } from 'antd';
import { CloseOutlined, EditOutlined } from '@ant-design/icons';
import { setItem, toggleUpdateModal } from '../../redux/todoSlice';
import TodoUpdater from '../TodoUpdater/TodoUpdater';

const TodoItem = (props) => {

  const dispatch = useDispatch()

  const { deleteTodoItem, updateTodoItemStatus } = useTodo()

  const { Text } = Typography

  async function handleFinishClick() {
    updateTodoItemStatus(props.id, !props.done)
  }

  async function handleDeleteClick() {
    deleteTodoItem(props.id)
  }

  function handleUpdateClick() {
    dispatch(toggleUpdateModal())
    dispatch(setItem({id: props.id, text: props.value}))
  }

  const item = useSelector((state) => state.todo.itemList.find(item => item.id === props.id))

  return (
    <>
      <span onClick={handleFinishClick}>
        {item?.done ? <Text className='item' delete>{props.value}</Text> : <Text className='item'>{props.value}</Text>}
      </span>
      <div className='controlBox'>
        <Button className='updateButton' type="primary" shape="circle" icon={<EditOutlined />} onClick={handleUpdateClick} />
        <Button danger shape="circle" icon={<CloseOutlined />} onClick={handleDeleteClick} />
      </div>
    </>
  )
}

export default TodoItem