import React from 'react'
import TodoItem from '../TodoItem'
import { useSelector } from 'react-redux'
import { List, Typography } from 'antd'
import './index.css'

const TodoGroup = () => {

  const itemList = useSelector((state) => state.todo.itemList)

  const { Title, Text } = Typography;

  return (
    <div className='list'>
      <List
        header={<span className='listHead'>Todo List</span>}
        bordered
        dataSource={itemList}
        renderItem={(item) => (
          <List.Item>
            <TodoItem value={item.text} key={item.id} id={item.id} done={item.done}></TodoItem>
          </List.Item>
        )}
      />
    </div>
  )
}

export default TodoGroup