import React, { useState } from 'react'
import './index.css';
import useTodo from '../../hooks/useTodo';
import { Button, Input, Select, Space, } from 'antd';

const TodoGenerator = () => {

  const { createTodoItem } = useTodo()

  const { TextArea } = Input;

  const [text, setText] = useState('')

  function handleInput(event) {
    setText(event.target.value)
  }

  async function handleAddItem() {
    console.log(text)
    if (text === '' || text === undefined) {
      alert('请输入内容')
    } else {
      createTodoItem(text)
      setText('')
    }
  }

  return (
    <div className='generator'>
      <TextArea
        className='inputText'
        showCount
        maxLength={50}
        style={{ height: 120, marginBottom: 24 }}
        onChange={handleInput}
        value={text}
        placeholder="input a new todo here..."
      />
      <Button className='add' type="primary" onClick={handleAddItem}>add</Button>
    </div >
  )
}

export default TodoGenerator