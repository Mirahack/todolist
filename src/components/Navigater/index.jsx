import React from 'react'
import { useNavigate } from 'react-router-dom'
import './index.css'
import { Menu } from 'antd';
import { HomeOutlined, FileDoneOutlined, QuestionOutlined } from '@ant-design/icons';

const Navigater = () => {

    function getItem(label, key, icon, children, type) {
        return {
            key,
            icon,
            children,
            label,
            type,
        };
    }

    const items = [
        getItem('Home', '1', <HomeOutlined />),
        getItem('Done', '2', <FileDoneOutlined />),
        getItem('Help', '3', <QuestionOutlined />)
    ];

    const nagivate = useNavigate()

    const onClick = (e) => {
        console.log('click ', e);
        switch (e.key) {
            case '1':
                nagivate(`/`)
                break;
            case '2':
                nagivate(`/done`)
                break;
            case '3':
                nagivate(`/help`)
                break;
            default:
                break;
        }
    };

    return (
        <Menu className='nav'
            onClick={onClick}
            style={{
                width: 256,
            }}
            defaultSelectedKeys={['1']}
            mode="inline"
            items={items}
        />
    )
}

export default Navigater