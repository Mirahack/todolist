import React, { useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { Button, Modal, Input } from 'antd';
import { setItem, toggleUpdateModal } from '../../redux/todoSlice';
import useTodo from '../../hooks/useTodo';

const TodoUpdater = () => {

    const { updateTodoItemText } = useTodo()

    const dispatch = useDispatch()

    const open = useSelector((state) => state.todo.open)

    const item = useSelector((state) => state.todo.item)

    const { TextArea } = Input;

    async function handleOk() {
        updateTodoItemText(item.id, item.text)
        dispatch(setItem({id: item.id, text: ''}))
        dispatch(toggleUpdateModal())
    };

    const handleCancel = () => {
        dispatch(setItem({id: item.id, text: ''}))
        dispatch(toggleUpdateModal())
    };

    function handleTextChange(event) {
        console.log("first", event.target.value)
        dispatch(setItem({id: item.id, text: event.target.value}))
    }

    return (
        <Modal
            title="update item"
            open={open}
            onOk={handleOk}
            onCancel={handleCancel}
            key={item.id}
        >
            <TextArea
                showCount
                maxLength={50}
                style={{ height: 120, marginBottom: 24 }}
                onChange={handleTextChange}
                defaultValue={item.text}
            />
        </Modal>
    )
}

export default TodoUpdater